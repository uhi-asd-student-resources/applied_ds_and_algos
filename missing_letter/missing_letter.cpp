// Problem 1: Missing Letters
// Code in this problem is pseudocode and you need to adapt it for your chosen language.
//
// Write a function:
//
//     char missing_letters(string a)
//  
// that, given a string with the letters of the alphabet returns the smallest letter missing.
//
// The letters will be either in uppercase or lowercase but not both.
//
// For example:
//
//     Given a = "abcdefgijklmnoprstuvwxyz" the function will return "h"
//     Given a = "" the function will return "a"
//     Given a = "abcdefghijklmnopqrstuvwxyz" the function will return ""
//     Given a = "abcdefghijklmnopqrstuvwxy" the function will return "z"
//
// ********* The string will always be in alphabetical order. *********
//    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// This means that the string is SORTED.  Without this you will ALWAYS need to sort your string
// somehow first! (This can include using a hashmap/dictionary structure of some kind if applicable. )


#include <bits/stdc++.h>            // this brings in ALL the C++ machinery we need to solve interview questions. DO NOT DO THIS IN REAL CODE
#include <cassert>                  // this brings in our assert library

using namespace std;                // this allows us to not write std:: all the time.  DO NOT DO THIS IN REAL CODE.

// char is a 8-bit signed value i.e. it goes from -128 to +127
// string is a class which hides the fact that it is in fact an array of char
char missing_letters(string user_input)
{
    // Always check for the empty string, the number of times I have failed a problem like this
    // because I forgot is sealed on my mind.
    if ( user_input.empty() ) return 'a';

    // This test is to see if we have all 26 characters
    // crucially because of the definition of the problem
    // we know we don't have strings like 'aaaaaaaaaaaaaaaaaaaaaa'
    // in which case this check would fail.
    // This highlights that you need to ensure your program takes account
    // of all the problem information.
    if ( user_input.size() == 26 ) return 0;

    // What happens if we had more than 26 letters what would we do?

    // We only want to loop through the alphabet once.  This does
    // at most 26 comparisons in this solution. (Not 26 * log(26))
    // Our N (if you read this weeks material) is 26 because this is 
    // our maximum input length that is a valid user input. SO
    // we can say this is O(N) (\big_theta(N)).

    // size_t is an unsigned int so the maximum length of a string can be the maximum value of 
    // unsigned int - 1, because string::npos (as you saw with Vittorio solution) has the value
    // of 4294967295 (0xFFFFFFFF).
    // NOTE you will get warnings if you try and compare an signed int and an unsigned int.
    // An unsigned int only takes positive values of zero and above.
    // A signed int take negative values using two's complement (read about this if required).

    // An alternative for loop construction would be:
    // for( auto ch : user_input ) {}
    // for( auto ch = user_input.begin(); ch != user_input.end(); ch++ ) {}
    //      here the .end() will return string::npos when it reaches the end of the string
    for( size_t ii=0; ii < user_input.size(); ii++ ) {

        // 'a' is 26 in ASCII
        // user_input[ii] is the character such as 00011010 = 26 = 'a'

        // (LowerCase Test) Input1 = user_input[ii] != 'a'+ii, when true we put 1, when false we put 0
        // (UpperCase Test) Input2 = user_input[ii] != 'A'+ii, when true we put 1, when false we put 0
        // 
        // We are evaluating an && expression, which has the following truth table:
        //      Input1  Input2          |       Output
        //       0        0             |         0
        //       1        0             |         0
        //       0        0             |         0
        //       1        1             |         1
        // i.e. the IF statement can only succeed when both inputs are 1 (true).
        // 
        // Working this through with an example:
        // user_input[ii] = 'b', 'a'+ii = 'b', so we want b != b to be false
        // Input1 is false as b does equal b
        // so user_input[ii] = 'b', 'A'+ii == 'B', so that is true because 'b' != 'B'
        // Input2 is true
        // But we have an AND binary expression so that we need both to be true
        // to enter the IF statement.  This is not so we can continue our loop.
        //
        // Our second case is if we have a 'B'
        // user_input[ii] = 'B', 'a'+ii = 'b', so we want B != b to be true
        // Input1 is true as B does equal b
        // so user_input[ii] = 'B', 'A'+ii == 'B', so that is false because 'B' != 'B' is false
        // Input2 is false
        // But we have an AND binary expression so that we need both to be true
        // to enter the IF statement.  This is not so we can continue our loop.
        //
        // Our third case is if we have a 'c' when we wanted to see 'b'
        // user_input[ii] = 'c', 'a'+ii = 'b', so we want c != b to be true
        // Input1 is true as c does equal b
        // so user_input[ii] = 'c', 'A'+ii == 'B', so that is false because 'c' != 'B' is true
        // Input2 is true
        // We have an AND binary expression so that we need both to be true
        // to enter the IF statement.  We ENTER the if statement
        
        // The single quotes here say to C++ interpret this letter as a 1 byte character
        // that we can then perform arithmetic operations on.
        if ( user_input[ii] != 'a'+ii && user_input[ii] != 'A'+ii ) {
            return 'a'+ii;
        }

        // this expression above is equivalent to the following:
        // we have the same inputs and we want to work through the same examples above
        //
        // Working this through with an example:
        // user_input[ii] = 'b', 'a'+ii = 'b', so we want b != b to be false
        // Input1 is false as b does equal b
        // we have a single condition so we continue.
        //
        // Our second case is if we have a 'B'
        // user_input[ii] = 'B', 'a'+ii = 'b', so we want B != b to be true
        // so we are going to into our first IF statement
        // so user_input[ii] = 'B', 'A'+ii == 'B', so that is false because 'B' != 'B' is false
        // so our second IF statement is FALSE and we continue the loop
        // 
        // Our third case is if we have a 'c' when we wanted to see 'b'
        // user_input[ii] = 'c', 'a'+ii = 'b', so we want c != b to be true
        // our first if statement passes
        // so user_input[ii] = 'c', 'A'+ii == 'B', so that is false because 'c' != 'B' is true
        // our second if statement passes
        // so we can ENTER the second IF and return our missing character
        
        // This second if statement is here only to highlight that the above expression 
        // is identical to this syntax.  it does not change the actual working of the 
        // program.
        if ( user_input[ii] != 'a'+ii ) { 
            if ( user_input[ii] != 'A'+ii ) {
                return 'a'+ii;
            }
        } 

    }
    return 'z';
}

int main()
{
    // ALWAYS put your examples as test cases in the code you are working on first.
    // THEN make the smallest change each time to make each test case work.
    // OTHERWISE you are doing more work, which costs more time and these
    // interviews are always time limited.
    //
    // If the test cases given are not representative of all possible user inputs,
    // in your opinion, then you should ALWAYS write more test cases (preferably) BEFORE
    // you start writing your solution.
    //
    // Why? Because in the immortal words of Bob Ross
    // 
    // Bob Ross - "there are happy little accidents"
    //
    // where your solution will already solve some edge cases and you may not realise it.

    {
    string ex1 = "abcdefgHijklmnopqrstuvwxy";
    assert( missing_letters(ex1) == 'z' || missing_letters(ex1) == 'Z' );
    }
    {
    string ex1 = "abcdefgHiklmnopqrstuvwxyz";
    assert( missing_letters(ex1) == 'j' || missing_letters(ex1) == 'J' );
    }
    {
    string ex1 = "";
    assert( missing_letters(ex1) == 'a' || missing_letters(ex1) == 'A' );
    }
    {
    string ex1 = "abcdefghijklmnopqrstuvwxyz";
    assert( missing_letters(ex1) == 0 );
    }
    {
    string ex1 = "abcdefghijklmnopqrstuvwxy";
    assert( missing_letters(ex1) == 'z' || missing_letters(ex1) == 'Z' );
    }
    {
    string ex1 = "ABCDEFGHIJKLMNOPQRSTUVWXY";
    assert( missing_letters(ex1) == 'Z' || missing_letters(ex1) == 'z' );
    }
    {
    // mixed case test (question was ambiguous so added this in)
    string ex1 = "abcdefGHIJKLMNopqrstuvwXY";
    assert( missing_letters(ex1) == 'Z' || missing_letters(ex1) == 'z' );
    }
    return 0;   
}