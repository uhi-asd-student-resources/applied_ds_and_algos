# "str" is a type, while in Python you don't HAVE to specify types, 
# it is good to try to do so as it catches bugs earlier.
# "def" stands for "define" declares a function
# I have kept this similar to the C++ version so you can see the
# similarities
def missing_letter(my_string : str):
    if len(my_string) == 0: return "a"
    if len(my_string) == 26: return ""
    # ch is a common abbreciation for character
    for ii, ch in enumerate(my_string):
        if ord(ch) != ord("a")+ii and ord(ch) != ord("A")+ii:
            return chr(ord('a')+ii)
    return "z"


# This is a more python-esque method.  Note that
# this is still only doing at most 26 comparisons
# and we have avoided doing effectively an another 
# set of work inside our loop.
# This method also has the advantage that it removes
# the order of the letters from being a factor.
# When might this method not work?
import string
def missing_letter_v2(my_string : str ):
    if len(my_string) == 0: return "a"
    if len(my_string) == 26: return ""
    # Use the predefined constant for alphabet from string module
    # explode into individual characters and then
    # use set to make unique and because we want to use 
    # the difference method later on.
    # From an efficiency point of view this is O(N).
    alphabet = set(string.ascii_lowercase)   
    # We also explode our given string into individual characters.
    # This is O(N).
    my_string = set(my_string.lower())       
    # We then use the difference method to get the difference
    # between our two sets.  Note that while we have 2 sets of characters we 
    # only need to travel once through our letters.
    # The difference of 2 sets can be done very efficiently so 
    # this will be worst case O(N) but most likely better.

    # TM: Had the line below, modified as an alternative:
    # these two do the same thing in different ways.
    # return "".join(set.difference(alphabet, my_string))
    return list(set.difference(alphabet, my_string))[0]
    # Note O(N) + O(N) + O(N) is still just O(N)

# This was a submission from a couple of students.
# This is the same to missing_letter_v2 except does not handle
# the zero case.
# This is a perfectly valid solution, once you add in the initial checks on the input,
# however it does hide the complexity of the solution within a simple minus sign.
def missing_letter_v3(input):
    # If you remove these two if statements we 
    # fail our tests.
    if len(input) == 0: return "a"
    if len(input) == 26: return ""

    #TM: remove sorted as there should only be one element anyway
    #return sorted(set(string.ascii_lowercase) - set(input.lower()))[0]
    #TM: list enables us to index into a set and that is all we need.
    return list(set(string.ascii_lowercase) - set(input.lower()))[0]


# Here are our tests that we are given
user_string = ""
assert missing_letter(user_string) == "a" 

user_string = "abcdefgijklmnopqrstuvwxyz"
assert missing_letter(user_string) == "h"

user_string = "abcdefghijklmnopqrstuvwxyz"
assert missing_letter(user_string) == ""

user_string = "abcdefghijklmnopqrstuvwxy"
assert missing_letter(user_string) == "z"



user_string = ""
assert missing_letter_v2(user_string) == "a" 

user_string = "abcdefgijklmnopqrstuvwxyz"
assert missing_letter_v2(user_string) == "h"

user_string = "abcdefghijklmnopqrstuvwxyz"
assert missing_letter_v2(user_string) == ""

user_string = "abcdefghijklmnopqrstuvwxy"
assert missing_letter_v2(user_string) == "z"


user_string = ""
assert missing_letter_v3(user_string) == "a" 

user_string = "abcdefgijklmnopqrstuvwxyz"
assert missing_letter_v3(user_string) == "h"

user_string = "abcdefghijklmnopqrstuvwxyz"
assert missing_letter_v3(user_string) == ""

user_string = "abcdefghijklmnopqrstuvwxy"
assert missing_letter_v3(user_string) == "z"
