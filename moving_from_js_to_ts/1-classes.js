//
// Define a class
// - class contains data and methods
//
// - class functions are called methods
// - class variables are called fields
//
// All classes inherit from Object
class ParentClass {
    static field3 = 5; // available to all ExampleClasses
    field2 = 20; // local to this instance class, public to the world

    constructor(name) {
        this.name = name;
        this.field1 = 10; // local to this instance after construction
        console.log(this.name+" # calling ParentClass::constructor")
    }

    method1() {
        console.log(this.name+" # calling ParentClass::method1 with field value "+this.field1)
    }
}

console.log(ParentClass.field3)        // prints 5
console.log(ParentClass.field2)        // prints undefined

// Create instances of a class using the keyword 'new'
const eg1 = new ParentClass("eg1");
const eg2 = new ParentClass("eg2");
eg2.field1 = 200;
eg1.method1();
eg2.method1();                  // demonstrate two instances hold different values
eg1.field2 = 100;            
console.log(ParentClass.field2)        // prints undefined
console.log(eg1.field2)                 // prints 100
console.log(eg2.field2)                 // prints 20

class ChildClass extends ParentClass {
    constructor(name) {
        super(name)
    }

    method1() {
        super.method1()
        console.log("called in child class")
    }
}

// this is annoying we were supposed to pass a value to this child class?
const child = new ChildClass()
child.method1()

const child2 = new ChildClass("childWithAName")
child2.method1()