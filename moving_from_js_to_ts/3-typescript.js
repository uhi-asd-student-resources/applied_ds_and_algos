"use strict";
// install using npm install typescript
// you can also install globally npm install --global typescript
// command is: ./node_modules/.bin/tsc
function should_take_a_number(x) {
    return x + 1;
}
console.log(should_take_a_number(1)); // 2
console.log(should_take_a_number("1")); // 11 - this is now an ERROR - YAY!
function should_take_an_undefined(y) {
    if (y == undefined) {
        console.log("yes, its undefined");
    }
    else {
        console.log("no, its undefined");
    }
}
let x; // we wouldn't really do this as its pretty useless
should_take_an_undefined(x); // takes undefined, passes
should_take_an_undefined(null); // takes null, this is now an error!
function should_take_a_callable(f) {
    f("here in should_take_a_callable");
}
should_take_a_callable(console.log);
should_take_a_callable(2); // this is now an error BEFORE we run - YAY!
