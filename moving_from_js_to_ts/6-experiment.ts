// experiment with seeing how javascript is translated to Javascript
// ./node_modules/.bin/tsc -t es6 6-experiment.ts
// ./node_modules/.bin/tsc -t es5 6-experiment.ts

function some_function_we_like_to_call() : void {
    const x = 5;
    let y = 6;
    var z = 10;
}

some_function_we_like_to_call()

