function should_take_a_number(x) {
    return x + 1
}

console.log(should_take_a_number(1))        // 2
console.log(should_take_a_number("1"))      // 11

function should_take_an_undefined(y) {
    if ( y == undefined ) {
        console.log("yes, its undefined")
    } else {
        console.log("no, its not undefined")
    }
}

let x
should_take_an_undefined(x)        // takes undefined, passes
should_take_an_undefined(null)     // takes null, passes - but we only wanted to pass undefined

function should_take_a_callable(f) {
    return f("here in should_take_a_callable")
}

should_take_a_callable(console.log)
function a() {
should_take_a_callable(2)               // note that this fails when we RAN the script and we reached this line, not before.  This could be deep in our code
}


// WHAT CAN WE DO????