// install using npm install typescript
// you can also install globally npm install --global typescript
// command is: ./node_modules/.bin/tsc

function should_take_a_number(x: number): number { 
    return x + 1
}

console.log(should_take_a_number(1))        // 2
// console.log(should_take_a_number("1"))      // 11 - this is now an ERROR - YAY!

function should_take_an_undefined(y: undefined) : void {
    if ( y == undefined ) {
        console.log("yes, its undefined")
    } else {
        console.log("no, its undefined")
    }
}

let x: undefined                    // we wouldn't really do this as its pretty useless
should_take_an_undefined(x)        // takes undefined, passes
// should_take_an_undefined(null)     // takes null, this is now an error!

function should_take_a_callable(f: Function): void { // note the capital F in Function
    f("here in should_take_a_callable")
}

should_take_a_callable(console.log)
// function a() {
// should_take_a_callable(2)               // this is now an error BEFORE we run - YAY!
// }

// let w = "a string"

// w = 9 