// What we have seen so far with Javascript and Promises has been around 
// the construction of abstractions over and above basic concept of functions.
// We have seen functions passed as a value and called as a "Callable" type.

// In this example we look at what is a value in Javascript and the different types
// associated with them.

// A value is a thing that the interpreter treats as a single entity.
// Each value has a type.  This type allows certain sets of functionality
// to apply to one value or another value.

// In Javascript, the interpreter will try and change the type of the value
// you have to the value required as best it can.  Rarely does it error when
// it might be important to you.  We call this DYNAMIC typing.

// In JS, we have ==   which compares VALUES
// In JS, we have ===  which compares VALUES AND TYPES

console.log("undefined\t\t"+typeof undefined)
console.log("null\t\t\t"+typeof null)

console.log("does undefined == null? "+(null == undefined))
console.log("does undefined === null? "+(null === undefined))

// Primitive Data Types are:
//  number, string, boolean, undefined
console.log("\n\nPrimitive Data Types")
console.log("10\t\t\t"+typeof 10)                      // number
console.log("1.0\t\t\t"+typeof 1.0)                     // number
console.log("\"hello\"\t\t\t"+typeof "hello")                 // string
console.log("'hello'\t\t\t"+typeof 'hello')                 // string
console.log("`hello`\t\t\t"+typeof `hello`)                 // string
console.log("true\t\t\t"+typeof(true))
console.log("false\t\t\t"+typeof(false))

let y   // type undefined
console.log("Unassigned variable is: "+y)


// Complex Data types are:
//  object, function
console.log("\n\nComplex Data Types")
console.log("[]\t\t\t\t"+typeof [])                      // object
console.log("{}\t\t\t\t"+typeof {})                      // object       
console.log("function() {}\t\t\t"+typeof function() {})           // anonymous function
console.log("() => {}\t\t\t"+typeof (() => {}))              // anonymous function
console.log("Array\t\t\t\t"+typeof Array)                   // object
const arr = new Array()
console.log("new Array\t\t\t"+typeof arr)             // function


console.log("Array == []\t\t\t"+(Array == []))  // <-- this now makes not sense so bug caught!

console.log("new Array == []\t\t\t"+(arr == []))
console.log("Module:    Math\t\t\t" +typeof Math)          // object
class ExampleClass {}
const eggs = new ExampleClass
console.log("Class Def: ExampleClass\t\t"+typeof ExampleClass)            // class        [ function ]
console.log("Instance:  eg1\t\t\t"+typeof eggs)                     // instance     [ object ]
console.log("Can an instance == a class def: "+(ExampleClass == eggs))
console.log("Can an instance === a class def: "+(ExampleClass === eggs))
console.log("Promise\t\t\t\t"+typeof Promise)                 // Promise      [ function ]
console.log("new Promise((r,e)={})\t\t"+typeof new Promise((r,e)=>{}))  // new Promise      [ object ]

