// Simple demo of how the Vue object model works
// (This is obviously a simplification!)

const template = "<html>{{msg}}</html>"

class VueDemo {
    constructor() {
        // create a local variable to this class that is accessible 
        // by commit and dispatch.
        this.context = {
            state: {
                msg: "greetings"
            },
            mutations: {
                changeSomething(state, payload) {
                    console.log("mutation called")
                    state.msg = payload.msg
                }
            },
            actions: {
                // not that the names in this object must be the same
                // as the key-value pairs in the passed object
                reactToEvent({ state, commitx }) {
                    console.log("action called")
                    const payload = { msg: "hello" }
                    commitx("changeSomething", payload)
                }
            }
        }
    }

    commit(mutationName, payload) {
        // if we did not have the closure around this earlier then the this
        // will point to the environment of reactToEvent.
        console.log("commit called")
        const userDefinedCommitFunction = this.context.mutations[mutationName]
        if (userDefinedCommitFunction) {
            userDefinedCommitFunction(this.context.state, payload)
        }
    }

    dispatch(actionName) {
        console.log("dispatch called")
        const userDefinedActionFunction = this.context.actions[actionName]
        if (typeof userDefinedActionFunction !== undefined) {
            // note we have to save our context here
            const me = this;
            // we wrap our commit function using the me variable.
            // this is called creating a closure
            // note we can pass MORE items in this object than we need, the order is not important.
            const ctxt = { "state": this.context.state, "extra": 1, "commitx": (m,p) => { me.commit(m,p) } }
            userDefinedActionFunction(ctxt)

            // render something here... to the webpage
            // by this point the state has been updated
            // obviously the rendering in vue is a more complex set of rules and
            // binding options
            console.log(template.replace("{{msg}}", this.context.state.msg))
        }
    }
}

v = new VueDemo();                      // create a new instance of our ViewDemo object
console.log(v.context.state.msg)        // print greetings
v.dispatch("reactToEvent")              // call our function via "dynamic" dispatching
console.log(v.context.state.msg)        // print hello