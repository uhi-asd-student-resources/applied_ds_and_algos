// In a programming language of your choice:
//
// Write a function to sum all the numbers from 1 to 100 _using a loop_.
//
// Write a function to use the following formula to calculate the same value:
//        
//             V = ( n / 2 ) * ( n + 1 ) 
//            
// Can you explain why this formula works?
//
// Write a function that uses a loop to sum all the first 100 even numbers.
//
// Write a function that modifies the equation V to sum the first n even numbers. For example, 
//
//     Given the argument value 1 the function should return 2.
//     Given the argument value 2 the function should return 6.
//     Given the argument value 3 the function should return 12.
//     Given the argument value 100 the function should return 10100.

public class Counting {
    public static int sum_all_numbers(int start, int end) {
        int sum = 0;
        for( int ii=start; ii <= end; ii++ ) {
            sum+=ii;
        }
        return sum;
    }

    public static int sum_all_even_numbers(int start, int end) {
        int sum = 0;
        for( int ii=start; ii <= end; ii++ ) { // psst: you can do better
            if ( ii % 2 == 0 ) {
                sum+=ii;
            }
        }
        return sum;
    }

    public static int sum_all_numbers_by_function(int end) {
        int result = ( end / 2 ) * ( end + 1 );
        return result;
    }

    public static int sum_all_even_numbers_by_function(int end) {
        int result = ( end ) * ( end + 1 );
        return result;
    }

    public static void main(String[] args) {
        System.out.println(sum_all_numbers(1,100)); 
        System.out.println(sum_all_numbers_by_function(100));
        System.out.println(sum_all_even_numbers(1,100));
        System.out.println(sum_all_even_numbers_by_function(50));
        System.out.println(sum_all_even_numbers_by_function(1));
        System.out.println(sum_all_even_numbers_by_function(2));
        System.out.println(sum_all_even_numbers_by_function(3));
        System.out.println(sum_all_even_numbers_by_function(100));
    }
}