import org.junit.Test;
import static org.junit.Assert.*;

public class CountingTest {

    @Test
    public void testCounting() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_numbers(0,100);
        assertEquals(5050, result);
    }

    @Test
    public void testCountingByFunction() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_numbers_by_function(100);
        assertEquals(5050, result);
    }

    @Test
    public void testCountingEven2() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers(0,2);
        assertEquals(2, result);
    }

    @Test
    public void testCountingEven4() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers(0,4);
        assertEquals(6, result);
    }

    @Test
    public void testCountingEven6() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers(0,6);
        assertEquals(12, result);
    }

    @Test
    public void testCountingEven() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers(0,100);
        assertEquals(2550, result);
    }

    @Test
    public void testCountingEvenByFunction1() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers_by_function(1);
        assertEquals(2, result);
    }
    @Test
    public void testCountingEvenByFunction2() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers_by_function(2);
        assertEquals(6, result);
    }
    @Test
    public void testCountingEvenByFunction3() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers_by_function(3);
        assertEquals(12, result);
    }

    @Test
    public void testCountingEvenByFunction() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers_by_function(50);
        assertEquals(2550, result);
    }

    @Test
    public void testCountingEven200() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers(0,200);
        assertEquals(10100, result);
    }

    @Test
    public void testCountingEven100() {
        Counting my_test = new Counting();
        int result = my_test.sum_all_even_numbers_by_function(100);
        assertEquals(10100, result);
    }
}