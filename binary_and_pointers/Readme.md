# Building C++ files

You need to compile each of these on your own machine.

The -o argument specifies the name of the executable.  If this is not given it will create a file called a.out overwriting any previous a.out file.

```
g++ array.cpp -o array
g++ class_lifecycle.cpp -o cls
g++ double-linked-list.cpp -o double-linked-list
g++ endian.cpp -o endian
g++ linkedlist.cpp -o linkedlist
g++ pointers.cpp -o pointers
```