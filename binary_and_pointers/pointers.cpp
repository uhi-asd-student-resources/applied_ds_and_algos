#include <iostream>

using namespace std;

// https://courses.engr.illinois.edu/cs225/sp2020/resources/stack-heap/

int *global_a = new int;
int global_b=100;


void called_a_function_by_value(int d) {
    cout << "called_a_function_by_value] Memory Address: " << &d << " Data:" << d << '\n';
}

// here d can be NULL!
void called_a_function_by_pointer(int* d) {
    cout << "called_a_function_by_pointer] Memory Address: " << d << " Data:" << *d << " Reference address: " << &d << '\n';
}

// this is preferred, and we will find out why in class_lifecycle....
// a reference cannot be NULL!
void called_a_function_by_reference(int& d) {  
    cout << "called_a_function_by_reference] Memory Address: " << &d << " Data:" << d << '\n';
}

int main() {
    // why does the stack start at 7FFFFFFF?????
    // what value does 8000000 represent?
    // why when 64-bit is 16 (hex digits) is the memory address only 48 bits (12 hex digits)?
    // https://en.wikipedia.org/wiki/X86-64#Canonical_form_addresses
    signed long highest_positive_ptr = 0x00007FFFFFFFFFFF;
    signed long highest_positive     = 0x7FFFFFFFFFFFFFFF;
    signed long is_next_positive     = 0x8000000000000000;
    signed long lowest_negative      = 0xF000000000000000;
    signed long lowest_positive      = 0x0000000000000000;
    signed long highest_negative     = 0xFFFFFFFFFFFFFFFF;
    
    cout << "Lowest Positive 64-bit number: " << lowest_positive << endl;
    cout << "Highest Positive 64-bit number: " << highest_positive << endl;
    cout << "0x8000000000000000: " << is_next_positive << endl;
    cout << "Highest Positive pointer: " << highest_positive_ptr << endl;
    
    cout << "Lowest Negative: " << lowest_negative << endl;
    cout << "Highest Negative: " << highest_negative << endl;

    cout << "\nVariables declared within the main function\n";

    int a = 5;          // declare on the stack (top of memory)
    cout << "a] Memory Address: " << &a << " Data:" << a << '\n';

    int* b = new int;   // declare on the heap (bottom in memory)
    *b = 7;
    cout << "b] Memory Address: " << b << " Data:" << *b << " Reference address: " << &b << '\n';

    int& c = a;         
    c = 10;
    cout << "c] Memory Address: " << &c << " Data:" << c << '\n';
    cout << "a] Memory Address: " << &a << " Data:" << a << '\n';

    c = *b;
    cout << "c] Memory Address: " << &c << " Data:" << c << '\n';
    cout << "b] Memory Address: " << b  << " Data:" << *b << " Reference address: " << &b << '\n';

    *b = 22;
    cout << "b] Memory Address: " << b  << " Data:" << *b << " Reference address: " << &b << '\n';
    cout << "c] Memory Address: " << &c << " Data:" << c << '\n';
    
    b = &a;         // ASSIGNING STACK MEMORY TO HEAP POINTER!!!! MEMORY LEAK
    cout << "b] Memory Address: " << b  << " Data:" << *b << " Reference address: " << &b << '\n';
    cout << "a] Memory Address: " << &a << " Data:" << a << '\n';

    cout << "\nWhat happens when variables are passed to a function...\n";

    int e = 10;
    int *f = new int;
    *f = 11;
    int &g = e;

    cout << "e] Memory Address: " << &e << " Data:" << e << '\n';
    cout << "f] Memory Address: " << f  << " Data:" << *f << " Reference address: " << &f << '\n';
    cout << "g] Memory Address: " << &g << " Data:" << g << '\n';

    cout << "Call by value: " << '\n';
    called_a_function_by_value(e);
    int alex_number = 1000;
    called_a_function_by_value(*f);
    called_a_function_by_value(g);
    cout << "Call by pointer: " << '\n';
    called_a_function_by_pointer(&e);
    called_a_function_by_pointer(f);
    called_a_function_by_pointer(&g);
    cout << "Call by reference: " << '\n';
    called_a_function_by_reference(e);
    called_a_function_by_reference(*f);
    called_a_function_by_reference(g);

    delete f;
    //delete b;


    cout << "\nGlobal variables...\n" << endl;

    // in .bss as it does not have a value yet
    cout << "global_a] memory address: " << global_a << " value: " << *global_a << " reference: " << &global_a << "\n";
    // in .data as it does have value
    cout << "global_b] memory address: " << &global_b << " value: " << global_b << "\n";


    delete global_a;
}