#include <iostream>

// doubly linked list
struct node
{
    int data;
    node *prev;     // allows us to go backwards
    node *next;     // allows us to go forwards
};

void add(node *node, struct node *&nodes)
{
    if (nodes == NULL)
    {
        nodes = node;
        return;
    }
    if (nodes->next == NULL)
    {
        nodes->next = node;
        node->prev = nodes;
        return;
    }
    add(node, nodes->next);
}

int main()
{
    node *nodes = NULL;
    int ii = 0;
    while (ii < 100000)
    {
        node *new_node = new node();
        new_node->next = NULL;
        new_node->prev = NULL;
        new_node->data = ii;
        add(new_node, nodes);
        ii++;
    }
    return 0;
}