#include <iostream>

// single direction linked list
struct node
{
    int data;
    node *next;     // allow us to move forward
};

void add(node *node, struct node *&nodes)
{
    if (nodes == NULL)
    {
        nodes = node;
        return;
    }
    if (nodes->next == NULL)
    {
        nodes->next = node;
        return;
    }
    add(node, nodes->next);
}

int main()
{
    node *nodes = NULL;
    int ii = 0;
    while (ii < 100000)
    {
        node *new_node = new node();
        new_node->data = ii;
        add(new_node, nodes);
        ii++;
    }
    return 0;
}