#include <iostream>

using namespace std;

// use g++ endian.cpp -o endian to build.

// example from https://en.wikipedia.org/wiki/Endianness
// $ hexdump -C endian | grep -A 2 Jo
// 00003050  a6 10 40 00 00 00 00 00  00 00 00 00 4a 6f 68 6e  |..@.........John|
// 00003060  00 00 00 00 6e 68 6f 4a  70 68 6f 4a 98 68 6f 4a  |....nhoJphoJ.hoJ|
// 00003070  48 68 6f 4a d1 de 94 4e  d1 de 94 4e 47 43 43 3a  |HhoJ...N...NGCC:|

// stored a byte at a time in order
// 00003030  00 00 00 00 4a 6f 68 6e  
char name[] = "John";

// little endian
//           00 00 00 00 6e 68 6f 4a
int name2 = 0x4A6F686E;

// not that this is not exact, there is precision
// 70 68 6f 4a
float name4 = 3.92246e+06;
float name5 = 3.92247e+06; // 98 68 6f 4a
float name6 = 3.92245e+06; // 48 68 6f 4a

// 1.24882e+09 1.24882e+09
// floating points have 6-7 digits of PRECISION
// accuracy is the DIFFERENCE of the value you get from the actual value required. |Observed - Actual|
float name7 = (float) 0x4A6F686E;

// d1 de 94 4e
// 1248815214.0 ===> standard form conversion ===> 1.24882 x 10^9
// 4e 94 de d1 = 124882
// 1 sign bit
// 8 exponent bits (10011101 = 1+4+8+16+128 = 157, 2^(157-127) = 2^30 = 1073741824 (1x10^9) )
// 23 mantissa bits ( [1].00101001101111011010001 = [1].1630498170852661 )
// Total Bits: 32
// mantissa * exponent = 1.07374182E09 * 1.1630498170852661
// result = 1248820000
float name3=1248815214.0;

int main() {
    cout << "A float has " << sizeof( float ) << " bytes\n";
    cout << "The value " << name3 << " is the nearest floating point representation of 1248815214.\n";

    cout << "0x4A6F686E has floating point value " << name7 << std::dec << '\n';

    // note that C/C++ will upgrade ints to floats, so we have to 
    // downgrade the float to an int.
    cout << "Accuracy of value is: " << ((int) name7 - name2) << "\n";

    cout << "Converting from little endian to a value\n";
    // our original bytes for J,o,h,n
    char bytes[] = { 0x6e, 0x68, 0x6f, 0x4a };
    int result = 0;
    for( int ii=0; ii < 4; ii++ ) {
        int shifted = ((int)(bytes[ii]) << (ii * 8));
        cout << "result: " << std::hex << result << " Byte: " << (int) bytes[ii] << " Shifted: " << shifted ;
        result |= shifted;
        cout << " OR=" << std::hex << result << "\n";
    }
    cout << "Integer: " << std::dec << result << " " << std::hex << result << std::dec << endl;
    float f = (float) result;
    cout << "Floating point: "<< f << std::endl;
}