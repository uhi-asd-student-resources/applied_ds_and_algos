#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int allocations = 0;
int frees = 0;

// normally you don't need to override these but we want to add some counters
// so we can track memory allocations.
// new and delete allocates on the HEAP
/*
void * operator new(size_t size) 
{ 
    allocations++;
    cout << "[+] Global 'new' operator called : index " << allocations << endl; 
    void * p = malloc(size); 
    return p; 
} 
  
void operator delete(void * p) 
{ 
    frees++;
    cout << "[-] Global 'delete' operator called : index " << frees << endl; 
    free(p); 
} 
*/

// We define a basic class which we are going to track the life of.

class ExampleClass 
{
    // by default stuff here is private
private:
    // only the same instance and instances of the same class can see this
protected:
    // children of this class can see this as well
public:
    // everyone can see this 

    std::string data;
    static int object_counter;
    int id;

    // basic constructor
    ExampleClass() {
        id = object_counter++;
        data = "new object "+to_string(id);
        cout << "[Instance " << id << "] ExampleClass::ExampleClass (constructor) " << id << endl;
    }

    // copy constructor
    ExampleClass(ExampleClass const & other) {
        id = object_counter++;
        cout << "[Instance " << id << "] ExampleClass::ExampleClass(&) (copy constructor)" << endl;
    }

    // move constructor for temporaries
    ExampleClass(ExampleClass&& other) {
        id = object_counter++;
        int original = this->id;
        this->data = other.data;
        this->id = other.id;
        cout << "[Instance " << id << "] ExampleClass::ExampleClass(&&) (move constructor): transfering from " << original << " to " << this->id << endl;        
    }

    // class destructor
    ~ExampleClass() {
        cout << "[Instance " << id << "] ExampleClass::~ExampleClass (destructor)" << endl;
    }

    // copy assignment
    ExampleClass& operator=(ExampleClass const & other) {
        cout << "[Instance " << id << "] ExampleClass::operator= (copy assignment)" << endl;
        return *this;
    }

    // aka member function
    // aka method
    // a function to print our class
    std::string print() const {
        stringstream ss;
        ss << "[Instance " << id << "] ExampleClass::print";
        return ss.str();
    }
}; // always remember the semicolon after the bracket

int ExampleClass::object_counter = 0;

std::ostream& operator<<( std::ostream& out, ExampleClass& item) {
    out << item.print();
    return out;
}

std::ostream& operator<<( std::ostream& out, ExampleClass const & item) {
    out << item.print();
    return out;
}

std::ostream& operator<<( std::ostream& out, ExampleClass* item) {
    out << *item;
    return out;
}


std::ostream& operator<<( std::ostream& out, ExampleClass const * item) {
    out << *item;
    return out;
}


void function_call_by_pointer(ExampleClass* ptr_to_class) {
    cout << "Calling by pointer\n" << ptr_to_class << endl;
    ptr_to_class = new ExampleClass(); // reassign the ptr_to_class
    cout << ptr_to_class << endl;
    // uncomment this if you are bothered about memory leaks
    delete ptr_to_class;
}

void function_call_by_double_pointer(ExampleClass** ptr_to_class ) {
    cout << "Calling by double pointer:\n" << *ptr_to_class << endl;
    delete *ptr_to_class; // we need to delete this object as otherwise we get a memory leak
    *ptr_to_class = new ExampleClass(); // reassign the ptr_to_class
    cout << *ptr_to_class << endl;
}

void function_call_by_pointer_to_const_class(ExampleClass const * ptr_to_class) {
    cout << "Calling by const pointer to object:\n" << ptr_to_class << endl;
    // uncomment to see error
    // ptr_to_class->data = "const ptr data";
}

void function_call_by_const_pointer_to_const_class(ExampleClass const  * const ptr_to_class) {
    cout << "Calling by const pointer to const object:\n" << ptr_to_class << endl;
    // uncomment to see error
    // ptr_to_class->data = "const ptr data";
}

void function_call_by_const_pointer_to_const_class(ExampleClass * const ptr_to_class) {
    cout << "Calling by const pointer to object:\n" << ptr_to_class << endl;
    ptr_to_class->data = "const ptr data";
    // we cannot reassign this though
    // ptr_to_class = new ExampleClass();
}

void function_call_by_reference(ExampleClass& ref_to_class) {
    cout << "Calling by reference to object: \n" << ref_to_class << endl;
    // uncomment to see error
    // ref_to_class->data = "hello";
    ref_to_class.data = "hello";
}

void function_call_by_reference_to_const_class(ExampleClass const & ref_to_class) {
    cout << "Calling by reference to const object:\n " << ref_to_class << endl;
    // uncomment to see error
    // ref_to_class->data = "hello";
    // ref_to_class.data = "hello";
}

void function_call_by_value(ExampleClass copy_of_object) {
    cout << "Calling by value (copy) to object: \n" << copy_of_object << endl;
    // uncomment to see error
    //copy_of_object->data = "hello";
    copy_of_object.data = "hello from copy"; // this value will never reach the calling function
}

void function_call_by_value_to_const_object(ExampleClass const copy_of_object) {
    cout << "Calling by value (copy) to object: \n" << copy_of_object << endl;
    // uncomment to see error
    // copy_of_object->data = "hello";
    // copy_of_object.data = "hello from copy";
}

// uncomment to see error
// ExampleClass& bad_return_reference() {
//     return ExampleClass();
// }

ExampleClass return_copy() {
    return ExampleClass();
}

ExampleClass* return_pointer() {
    auto a = new ExampleClass();
    return a;
}

// uncomment for error
// ExampleClass* return_pointer() {
//     auto a = ExampleClass();
//     return &a;
// }

int main() {
    {
        // STACK MEMORY EXAMPLES
        cout << "\n\n" << "STACK EXAMPLES\n\n";
        auto a = ExampleClass {};               // create a new object
        cout << "\n";
        auto b = ExampleClass(a);               // copy an object
        cout << "\n";
        auto c = ExampleClass(std::move(ExampleClass()));
        cout << "\n";
        c = a;                             // copy an object by assignment
        cout << "\n";
        // no need to delete as they will be cleaned up

        { // define a new scope
            auto z = ExampleClass {};
        } // z will be deleted here


        cout << "\n\nCalling functions with stack objects\n\n";

        // CALLING BY VALUE
        cout << "CALL BY VALUE" << std::endl;
        cout << "DATA: " << a.data << endl;
        function_call_by_value(a);
        cout << "DATA: " << a.data << endl;
        cout << "\n";
        cout << a.data << endl;
        function_call_by_value_to_const_object(a);
        cout << a.data << endl;
        cout << "\n";


        // CALLING BY POINTER, NO COPY IS MADE BUT REQUIRES HEAP OBJECT
        function_call_by_pointer(&a);
        // note the copy_cls is not modified as although the pointer
        // was changed in function_call_by_pointer the value of the 
        // pointer was copied
        cout << a << endl;
        cout << "\n";
        // uncomment for error
        // function_call_by_double_pointer(a); // makes no sense
        // cout << a << endl;

        function_call_by_pointer_to_const_class(&a);
        cout << "\n";
        function_call_by_const_pointer_to_const_class(&a);
        cout << "\n";

        // CALLING BY REFERENCE, NO COPY IS MADE
        cout << a.data << endl;
        function_call_by_reference(a);
        cout << a.data << endl;
        cout << "\n";
        function_call_by_reference_to_const_class(a);
        cout << a.data << endl;
        cout << "\n";

        
    }

    // HEAP MEMORY EXAMPLES
    {
        cout << "\n\n" << "HEAP EXAMPLES\n\n";
        auto cls = new ExampleClass();
        cout << cls << endl;

        cout << "\n";
        auto copy_cls = new ExampleClass(*cls);
        cout << copy_cls << endl;
        cout << "This original is still here: " << cls << endl;

        cout << "\n";
        auto move_constr = new ExampleClass(std::move(ExampleClass()));
        cout << move_constr << endl;

        cout << "\n";
        *move_constr = *cls;                // copy assignment called

        cout << "\n";
        delete move_constr;
        move_constr = nullptr;

        cout << "\n";
        move_constr = cls;                  // copying pointer to move_constr, copy assignment NOT called
        cout << move_constr << endl;

        cout << "\n";
        delete move_constr;
        // uncomment for crash
        // delete cls;                         // double delete

        cout << "\n\nCalling functions with heap objects" << endl;

        // CALLING BY POINTER, NO COPY IS MADE BUT REQUIRES HEAP OBJECT
        function_call_by_pointer(copy_cls);
        // note the copy_cls is not modified as although the pointer
        // was changed in function_call_by_pointer the value of the 
        // pointer was copied
        cout << copy_cls << endl;

        function_call_by_double_pointer(&copy_cls);
        cout << copy_cls << endl;

        function_call_by_pointer_to_const_class(copy_cls);
        function_call_by_const_pointer_to_const_class(copy_cls);

        // CALLING BY REFERENCE, NO COPY IS MADE
        cout << copy_cls->data << endl;
        function_call_by_reference(*copy_cls);
        cout << copy_cls->data << endl;
        function_call_by_reference_to_const_class(*copy_cls);
        cout << copy_cls->data << endl;

        // CALLING BY VALUE
        cout << copy_cls->data << endl;
        function_call_by_value(*copy_cls);
        cout << copy_cls->data << endl;
        function_call_by_value_to_const_object(*copy_cls);
        cout << copy_cls->data << endl;

        delete copy_cls;
    }

    {
        cout << "\n\nRETURNING OBJECTS\n\n";

        auto eg1 = return_copy();
        cout << eg1 << endl;

        cout << "\n";
        auto eg2 = return_pointer();
        cout << eg2 << endl;
        // if we let this go this is a memory leak
        delete eg2;
    }


    cout << "\n\nMEMORY ALLOCATIONS\n\n";
    cout << "Allocations: " << to_string(allocations) << "\n";
    cout << "Frees      : " << to_string(frees) << "\n";
}