// Read through this C program.
//
// When working correctly the program should print out zero 10 times. 
// It should then set the values of the array to its index.  
// Finally it should print out all ten values in reverse order.
//
// Compile the following C program using the following command:
//
//     gcc myapp.c -o myapp
//
// Run myapp on the command line or in your visual code terminal.  
//
// 1. What happens and why?  
// 2. Can you fix the bugs?
//
// ---- COPY AND PASTE THE FOLLOWING LINES INTO A NEW FILE ----
//
// Use Compiler Explorer to find out what is happening underneath?

#include <stdio.h>

int main() {
    int values[10];                     // <<------------ initialised?
    int ii=0;
    
    for(ii=0; ii < 10; ii++ ) {
        printf("%d: %d\n", ii, values[ii]); // <<-------- why does this produce strange values?
    }
    

    for(ii=0; ii < 20; ii++ ) {         // <<------------ how long is the array?
        values[ii] = ii;
    }
    

    // Currently loops down from : 10,9,8,7,6,5,4,3,2,1
    // Should be: 9,8,7,6,5,4,3,2,1,0
    for(ii=10; ii > 0; ii-- ) {         // <<------------ what are the bounds on this loop?
        printf("%d: %d\n", ii, values[ii]);
    }
    
    return 0;
}

