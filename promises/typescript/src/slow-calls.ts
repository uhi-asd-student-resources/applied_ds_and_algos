async function getName() {
  let ii : number = 0
  while( ii < 1000000000 ) {
    ii++
  }
  return "Jill"
}

async function getSurname() {
  let ii : number = 0
  while (ii < 1000000000 ) {
    ii++
  }
  return "Anderson"
}

async function getFullName() {
  let firstname = await getName();  // block
  let surname = await getSurname(); // block
  console.log(`${firstname} ${surname}`)
}

getFullName();
