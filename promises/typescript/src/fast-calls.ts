async function getName2() {
    let ii : number = 0
    while( ii < 1000000000 ) {
      ii++
    }
    return "Jill"
  }
  
  async function getSurname2() {
    let ii : number = 0
    while (ii < 1000000000) {
      ii++
    }
    return "Anderson"
  }
  
  // here the calls are done simultaneously
  async function getFullName2() {
    let [ firstname, surname ] = await Promise.all( [getName2(), getSurname2()] );
    console.log(`${firstname} ${surname}`)
  }
  
  getFullName2();
  