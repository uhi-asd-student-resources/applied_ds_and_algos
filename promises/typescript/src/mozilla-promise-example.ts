// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
// tsc --lib 'dom','es2018' mozilla-promise-example.ts


// To experiment with error handling, "threshold" values cause errors randomly
const THRESHOLD_A:number = 8; // can use zero 0 to guarantee error


// A function that does some work and on success will call the resolve function
// and on failure will call the reject function.
// resolve : a function that takes 1 argument
// reject : a function that takes 1 argument
function tetheredGetNumber(resolve : Function, reject : Function): void {
    console.log("TetheredGetNumber being called")
    try {
        setTimeout(
            function ():void {
                console.log("callback from timeout being called")
                const randomInt = Date.now();
                const value = randomInt % 10;
                try {
                    if (value >= THRESHOLD_A) {
                        throw new Error(`Too large: ${value}`);
                    }
                } catch (msg) {
                    reject(`Error in callback ${msg}`);
                }
                resolve(value);
                return;
            }, 500);
        // To experiment with error at set-up, uncomment the following 'throw'.
        // throw new Error("Bad setup");
    } catch (err) {
        reject(`Error during setup: ${err}`);
    }
    return;
}

function determineParity(value: number):any {
    const isOdd = value % 2 ? true : false;
    const parityInfo = { theNumber: value, isOdd: isOdd };
    return parityInfo;
}

function troubleWithGetNumber(reason: number):never {
    console.error(`Trouble getting number: ${reason}`);
    throw -999; // must "throw" something, to maintain error state down the chain
}

// !! Note that we cannot pass the type object here
// or we get error messages about fields missing.
// The type of the Promise here is the type of the value passed to the resolve function
// in this case its the type of parityInfo, which is a plain old JSON object.
function promiseGetWord(parityInfo: any): Promise<any> {
    console.log("promiseGetWord being called")
    // The "tetheredGetWord()" function gets "parityInfo" as closure variable.
    // More info on closures can be found at
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures

    // again resolve and reject are going to be functions
    var tetheredGetWord = function (resolve: Function, reject: Function) {
        console.log("tetheredGetWord being called")
        const theNumber: number = parityInfo.theNumber; // parityInfo comes from the surrouding state 
        const threshold_B: number = THRESHOLD_A - 1;
        if (theNumber >= threshold_B) {
            reject(`Still too large: ${theNumber}`);
        } else {
            parityInfo.wordEvenOdd = parityInfo.isOdd ? 'odd' : 'even';
            resolve(parityInfo);
        }
        return;
    }
    // this either needs to be a promise or something immediately evaluatable
    console.log("tetheredGetWord is: ")
    console.log(tetheredGetWord)
    return new Promise(tetheredGetWord);
}

// finally the promise
// tetheredGetNumber is a function that is called when the Promise executes
const x = new Promise(tetheredGetNumber)
    // this is passing 2 function for resolve and reject
    .then(determineParity, troubleWithGetNumber)
    // then when we are done we can pass in another promise
    .then(promiseGetWord)
    .then((info: any):any => {
        console.log("Got: ", info.theNumber, " , ", info.wordEvenOdd);
        return info;
    })
    .catch((reason) => {
        if (reason === -999) {
            console.error("Had previously handled error");
        }
        else {
            console.error(`Trouble with promiseGetWord(): ${reason}`);
        }
    })
    // https://mariusschulz.com/blog/using-promise-prototype-finally-in-typescript
    .finally(() => console.log("All done"));
