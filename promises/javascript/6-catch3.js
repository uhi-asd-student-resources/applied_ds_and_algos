
async function longTask() {
    let ii=0; 
    while( ii < 50000000) {
        ii++;
    }
    throw new Error("ahhhhh!!!!");
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
longTask()
.then(
    () => console.log("after instruction "+Date.now())
).catch(    // note here we have a catch clause to 'catch our error'
    (err) => {
        console.log("caught you!") 
        // console.log(err) // uncomment to see actual exception, not very interesting...
    }
);

// expecting this NOT to throw an exception ...