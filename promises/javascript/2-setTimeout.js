
function longTask() {
    let ii=0; 
    while( ii < 5000000000) {
        ii++;
    }
    console.log("done!")
    return 10;
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
setTimeout(longTask, 0);

// another way to express the same thing
setTimeout(() => { 
    let ii=0; 
    while( ii < 5000000000) {
        ii++;
    }
    console.log("done!")
}, 0)

// and another
setTimeout(function() { 
    let ii=0; 
    while( ii < 5000000000) {
        ii++;
    }
    console.log("done!")
}, 0)

console.log("after instruction "+Date.now())


