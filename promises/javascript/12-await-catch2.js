
async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( n == 50 ) {
        throw new Error("invalid value")
    }
    return n;
}

async function longerTask() {
    let a = await longTask(100)
    console.log("a was called")
    let b = await longTask(50)
    console.log("b was called")
    let c = a + b
    return c
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
longerTask().then(res => {
    console.log(res)
    console.log("after instruction "+Date.now())
}).catch( err => console.log("ahhhh! but caught nicely"))

// the difference here is that when we throw the Error we are no
// longer in danger of executing THEN clauses accidentally