// Beware the following:

// compare value only
// this will pass as the values are interpreted as the same
console.log(9=="9")

// compare type and value
// this will fail as types are different number is not the same as string
console.log(9==="9")
