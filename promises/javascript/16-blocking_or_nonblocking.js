// this is a blocking call.
// once called it hogs the single thread of execution until it completes.
function blocking_longtask() {
    let ii=0; 
    while( ii < 5000000000) {
        ii++;
    }
    console.log("done!")
}

// this is a nonblocking call, as it returns immediately.
// and other stuff can happen for the 4 seconds until this
// get called.  This is to simulate a network call or something similar.
function nonblocking_longtask() {
    setTimeout(() => console.log("Done!"), 4000)
}

// Note that I am abusing the terms blocking and nonblocking here
// for those of you with some knowledge of Threads and IO.
// For now though this is a reasonable way to think about things.