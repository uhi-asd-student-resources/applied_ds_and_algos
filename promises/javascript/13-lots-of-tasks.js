async function longTask2(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( -n == 50 ) {
        throw new Error("invalid negative value")
    }
    return -n;
}

async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( n == 50 ) {
        throw new Error("invalid value")
    }
    return n;
}

async function longerTask() {
    let a = await longTask(100)
    console.log("a was called")
    let b = await longTask(50)
    console.log("b was called")
    let c = a + b
    return c
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
const x = longerTask()
.then(longTask2)
.then((res) => {
    console.log(res)
    console.log("after instruction "+Date.now())
    return 10;
})
.then(longTask2)
// this will be executed here
.catch( err => console.log("ahhhh! but caught nicely"))

// what is x?
console.log(x)

x
.then(res => console.log("here! "+res))
