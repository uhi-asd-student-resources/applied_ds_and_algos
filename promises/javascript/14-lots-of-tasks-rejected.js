
async function longTask2(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( -n == 50 ) {
        throw new Error("invalid negative value")
    }
    return -n;
}

async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( n == 50 ) {
        throw new Error("invalid value") // <------ here the error is generated
    }
    return n;
}

async function longerTask() {
    let a = await longTask(100)
    console.log("a was called")
    let b = await longTask(50)      // <-------- this is where the Error is forced to occur
    console.log("b was called")
    let c = a + b
    return c
}

async function ret_function_example() {
    return () => { return 10; }
}

function ret_function_example_2() {
    return () => { return 11; }
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
const x = longerTask()
.then(longTask2) // passing the value of longTask2, not calling longTask2
.then(res => { // passing the value of the anonymous funciton, not calling the anonymous function
    console.log(res)
    console.log("after instruction "+Date.now())
    return 10;
}, rej => { console.log("here in rejected"); return 1000; }) // we come out here as we failed
.then(longTask2) // this is called because we only REJECTED we did not let the catch handler get called on error
// this will NOT be executed here because the rejection function
// succeeded
.catch( err => console.log("ahhhh! but caught nicely"))

// what is x?
console.log(x)

x
.then(res => console.log("here! "+res))

// got to here no more executable but there is stuff
// in the "then" queue


