
async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( n == 50 ) {
        throw new Error("invalid value")
    }
    return n;
}

async function longerTask() {
    let a = await longTask(100)
    console.log("a was called")
    let b = await longTask(50)
    console.log("b was called")
    let c = a + b
    return c
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
// the THEN clause actually takes 2 functions a "resolve" and a "reject" function.
// what happens if we do this...
longerTask().then(res => {
    console.log(res)
    console.log("after instruction "+Date.now())
}, rej => console.log("ahhhh!"))

// the rej function will be called on Error 
// BUT will this be treated as an error?
// What is going on here is that effectively you can think of this as
// if ( task is successful )
//      call resolve function
// else
//      call reject function
// call next THEN function...

