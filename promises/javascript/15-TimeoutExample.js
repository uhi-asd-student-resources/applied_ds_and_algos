// A question was asked about the following scenario:
//    a long task it kicked off
//    if the task takes longer than X amount of time, then thats an error
// How do you do this with promises?

// The immediate answer was non-obvious and many modules take care of this anyway
// so its rare to have to do this yourself.  Most commands will take a timeout
// value that you can set.

// We look on stackoverflow and came across something that looked like this:
function some_long_workload(timeout_ms, callback) {
    return new Promise(function(resolve, reject) {
        // Set up the real work
        callback(resolve, reject);

        // Set up the timeout
        setTimeout(function() {
            reject('Promise timed out after ' + timeout_ms + ' ms');
        }, timeout_ms);
    });
}

// The question is if we run this and the timeout occurs, is the long piece of work
// interrupted or does it continue but nothing happens afterwards.
// Lets find out:
function callback(resolve, reject) {
  setTimeout(function() { 
    // note that this STILL fires even though the reject function has been called in the 
    // promise above -- this is NOT what we want.
    console.log("callback timeout has fired")
    resolve
  }, 1000)
}

// use your debugger to trace this through
const x = some_long_workload(100, callback)
x.then(() => {
  console.log("after the timeout fires")
}).catch(() => {
  // we end up here as the reject function fires
  console.log("exception caught when timed out")
})

// so far we have this situation:
// kick off downloading file
// setTimeout for 3 minutes
// at 3 minutes, call reject, which will then move to next "Then" clause or exception 
//          AND blocks anything happening after the resolve completes
//          [AND the callback is cancelled.]  <-- this is what we are missing in our current implementation
// when the resolve completes, but it has nothing left to do

// this is where Promise.race comes in - thank you to another student for finding this.
const promise1 = new Promise((resolve, reject) => {
  setTimeout(function(a) {
    console.log("promise1 fired!")
    resolve(a)
  }, 1000, 'one'); // your long task
});

const promise2 = new Promise((resolve, reject) => {
  setTimeout(function(b) {
    console.log("promise2 fired!")
    resolve(b)
  }, 100, 'two'); // timeout
});

// race will KILL everything after the first promise returns
Promise.race([promise1, promise2]).then((value) => {
  console.log(value);
  // Both resolve, but promise2 is faster
});
// expected output: "two"

// Notice here that the work done in the timeout of Promise one DOES COMPLETE but that the THEN clause
// does NOT fire.  
// Therefore this is slightly better as when promise1 finished we don't also run the THEN clause
// but its not great as our blocking function IS STILL there.