// So now what happens when one of our await clauses throws an error

async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( n == 50 ) {
        throw new Error("invalid value")
    }
    return n;
}

async function longerTask() {
    let a = await longTask(100)
    let b = await longTask(50)          // this one will throw the Error
    let c = a + b
    return c
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
longerTask().then(res => {
    console.log(res)
    console.log("after instruction "+Date.now())
})

// this will throw an error