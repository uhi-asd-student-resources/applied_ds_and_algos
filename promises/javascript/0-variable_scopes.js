// After an interesting question from Vittorio we 
// did some digging and Jordan found the spec for this
// in Node.

// hello will be found in the scope 'global'
// in the browser, this would be automatically
// in the window object
// https://nodejs.org/api/globals.html
// "In browsers, the top-level scope is the global scope. 
// This means that within the browser var something will 
// define a new global variable. In Node.js this is different. 
// The top-level scope is not the global scope; 
// var something inside a Node.js module will be local to that module".

// Tip: run this in your VS Code debugger and notice there are two scopes:
// Local and Global.  Follow through where variables appear and disappear.

// 1. If you define without a keyword let,var,const then it will
// add the function/variable to the Global object.
global_hello = () => {
    console.log("hello from Global")
}
console.log(global.global_hello)

// find this variable in the debugger.
global_variable_badge = 10;

// The let keyword tells the node interpreter
// to place this function in the local scope area.
let local_hello_using_let = () => {
    console.log("hello from local scope using let")
}

// this will be undefined
console.log(global.local_hello_using_let)
// this will not be undefined
console.log(local_hello_using_let)

// this is the same thing with var
var local_hello_using_var = () => {
    console.log("hello from local scope using var")
}
// this will be undefined
console.log(global.local_hello_using_var)
// this will not be undefined
console.log(local_hello_using_var)


// this is the same thing with const
var local_hello_using_const = () => {
    console.log("hello from local scope using const")
}
// this will be undefined
console.log(global.local_hello_using_const)
// this will not be undefined
console.log(local_hello_using_const)


// now the real mind-bender is that 
// if you write something like this:
function can_I_see_x() {
    console.log("I can see x in a different function from where is was declared: "+x)
}
// this no longer works.
function declare_x_in_one_function() {
     x = 10;
     // find where x is declared - is it local or global?
     console.log("is x local or globally scoped?")
     can_I_see_x()
}
declare_x_in_one_function()
try {
console.log(x)
} catch(err) {
    console.log("we cannot see x, as it got removed by the end of the function scope")
}

// NOTE: DO NOT WRITE CODE THAT HAS THIS TYPE OF THING - THIS REALLY IS SPAGHETTI CODE.
// ALWAYS: use let or const to make it clear what scope the variables are in.


// A better way to write this is:

function can_I_see_x_as_an_argument(y) {
    console.log("I can see x in a different function from where is was declared by passing it as an argument: "+y)
}
// this no longer works.
function declare_x_in_one_function() {
     const x = 10;
     // find where x is declared - is it local or global?
     console.log("is x local or globally scoped?")
     can_I_see_x_as_an_argument(x)
}
declare_x_in_one_function()
try {
    console.log(x)
} catch(err) {
    console.log("we cannot see x, as it got removed by the end of the function scope")
}


