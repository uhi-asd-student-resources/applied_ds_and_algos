// A Promise is an object that is used as a placeholder for the 
// eventual results of a deferred (and possibly asynchronous) 
// computation.
// - What is a deferred computation?
// - What is an asynchronous computation?



// https://stackoverflow.com/questions/40922531/how-to-check-if-a-javascript-function-is-a-constructor
function isConstructable( fn ) {
    try {
        // String : the function that converts an object to a string e.g. String(x1)
        // [] : the arguments to pass to String
        // fn : the object to construct
        Reflect.construct(String, [], fn)
        return true;
    } catch( e ) {
        return false;
    }
}

// console.log(isConstructable(9) === false)
// console.log(isConstructable({}) === false )
// console.log(isConstructable({ constructor(x) { console.log(x) } }) === false )
// console.log(isConstructable(DummyImpl))

class MyPromise
{

    // executor is the function which has the workload in
    // it takes as arguments the onSuccess and onError
    // functions.
    constructor(executor) {
        this.executor = executor

        // common state to all promises
        this.state = "Pending"
        this.fulfillActions = []
        this.rejectActions = []
        this.promiseIsHandled = false;

        let me = this;
        this.onSuccess = (data) => {
            me.state = "Resolved"
            return data;
        }
        this.onError = (err) => {
            me.state = "Rejected"
            console.log(err)
        }
    }

    newPromiseCapability( fn ) {
        if ( isConstructable(fn) === false ) {
            throw TypeError("fn must be constructable");
        }
        let promiseCapability = {
            promise: undefined,
            resolve: undefined,
            reject: undefined
        }
    }

    isPending() {
        return this.state === "Pending"
    }

    isSettled() {
        return this.state !== "Pending"
    }

    isLockedInToAnotherPromise() {
        // ?
    }

    isResolved() {
        return this.isSettled() || this.isLockedInToAnotherPromise();
    }

    isUnresolved() {
        return !this.isResolved();
    }

    then(onSuccess, onError) {
        let me = this;
        this.onSuccess = () => { 
            me.state = "Fufilled"
            return onSuccess();
        }
        this.onError = () => {
            me.state = "Rejected"
            onError()
        }
    }

    catch(onError) {
        this.onError = () => {
            me.state = "Rejected"
            onError()
        }
    }

    finally(finallyFunction) {
        this.finallyFunction = finallyFunction
    }

}

function workload() {
    setTimeout(() => { console.log("done!")}, 1000);
}
const x = new MyPromise(workload);
