// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise

// enables strict mode, and will stop you from doing dodgy things like
// undeclared variables.
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
"use strict";

// To experiment with error handling, "threshold" values cause errors randomly
const THRESHOLD_A = 8; // can use zero 0 to guarantee error


// A function that does some work and on success will call the resolve function
// and on failure will call the reject function.
// resolve : a function that takes 1 argument
// reject : a function that takes 1 argument
function tetheredGetNumber(resolve, reject) {
    console.log("TetheredGetNumber being called")
    try {
        setTimeout(
            function () {
                console.log("callback from timeout being called")
                const randomInt = Date.now();
                const value = randomInt % 10;
                try {
                    if (value >= THRESHOLD_A) {
                        throw new Error(`Too large: ${value}`);
                    }
                } catch (msg) {
                    reject(`Error in callback ${msg}`);
                }
                resolve(value);
                return;
            }, 500);
        // To experiment with error at set-up, uncomment the following 'throw'.
        // throw new Error("Bad setup");
    } catch (err) {
        reject(`Error during setup: ${err}`);
    }
    return;
}

function determineParity(value) {
    const isOdd = value % 2 ? true : false;
    const parityInfo = { theNumber: value, isOdd: isOdd };
    return parityInfo;
}

function troubleWithGetNumber(reason) {
    console.error(`Trouble getting number: ${reason}`);
    throw -999; // must "throw" something, to maintain error state down the chain
}

function promiseGetWord(parityInfo) {
    console.log("promiseGetWord being called")
    // The "tetheredGetWord()" function gets "parityInfo" as closure variable.
    // More info on closures can be found at
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures

    // again resolve and reject are going to be functions
    var tetheredGetWord = function (resolve, reject) {
        console.log("tetheredGetWord being called")
        const theNumber = parityInfo.theNumber; // parityInfo comes from the surrouding state 
        const threshold_B = THRESHOLD_A - 1;
        if (theNumber >= threshold_B) {
            reject(`Still too large: ${theNumber}`);
        } else {
            parityInfo.wordEvenOdd = parityInfo.isOdd ? 'odd' : 'even';
            resolve(parityInfo);
        }
        return;
    }
    // this either needs to be a promise or something immediately evaluatable
    console.log("tetheredGetWord is: ")
    console.log(tetheredGetWord)
    return new Promise(tetheredGetWord);
}

// finally the promise
// tetheredGetNumber is a function that is called when the Promise executes
const x = new Promise(tetheredGetNumber)
    // this is passing 2 function for resolve and reject
    .then(determineParity, troubleWithGetNumber)
    // then when we are done we can pass in another promise
    .then(promiseGetWord)
    .then((info) => {
        console.log("Got: ", info.theNumber, " , ", info.wordEvenOdd);
        return info;
    })
    .catch((reason) => {
        if (reason === -999) {
            console.error("Had previously handled error");
        }
        else {
            console.error(`Trouble with promiseGetWord(): ${reason}`);
        }
    })
    .finally((info) => console.log("All done"));
