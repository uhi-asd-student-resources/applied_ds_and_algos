async function getName2() {
    // console.log(Date.now())
    let ii  = 0
    while( ii < 1000000000 ) {
      ii++
    }
    return "Jill"
    
    // setTimeout( () => "Jill", 5000 );
  }
  
  async function getSurname2() {
    // console.log(Date.now())
    let ii  = 0
    while (ii < 1000000000) {
      ii++
    }
    return "Anderson"
    
    // setTimeout( () => "Anderson", 5000 );
  }
  
  
  async function getFullName() {
    console.log(Date.now())
    let firstname = await getName2();  // block
    console.log(Date.now())
    let surname = await getSurname2(); // block
    console.log(Date.now())
    console.log(`${firstname} ${surname}`)
  }
  
  getFullName();
  