
// console.log("Variable x has value: ");
// console.log(x)

// // argument to promise must be a function
// //const y = new Promise(10);

// function f1() { console.log("f1 is executed"); }
// const z1 = new Promise(f1);

// // a is the empty function
// function f2(a) { console.log("f2 is executed"); console.log(a); a(); }
// const z2 = new Promise(f2);

// function f2a(a) { console.log("f2a is executed"); console.log(a); a(); }
// function f3(a) { console.log("f3 is executed"); console.log(a); }
// const z3 = new Promise(f2a).then(f3);

// function f2b(a) { console.log("f2b is executed"); console.log(a); a(); }
// function f4(a) { return new Promise((a) => { console.log("f4 is executed"); console.log(a); }); }
// const z4 = new Promise(f2b).then(f4);

// console.log("async always returns a promise...")

// async function call_me_1() {
//     console.log("call_me_1");
// }

// const z = call_me_1();
// console.log(z);

// console.log("await will wait until a promise settles, 'stops evaluation'")

// async function await_with_me() {
//     let promise = new Promise( (req, err ) => {
//         console.log("setting up timeout 1")
//         //setTimeout(() => req("done!"), 1000)
//         req(10);
//         err(10);
//     }).catch( (err) => { console.log("error: "+err) })
//     let result = await promise;
//     // this is now undefined as req was not passed anything
//     console.log(result);
// }

// await_with_me();

// async function await_with_me_2() {
//     let promise = await (new Promise( (req, err ) => {
//         console.log("setting up timeout 2")
//         setTimeout(() => req("done again!"), 2000)
//     }));
//     console.log(promise);
// }

// await_with_me_2();

// Why does it not make sense to use await in synchronous functions?
// Javascript is essentially a single job running thread.
// Every job MUST run to completion - as part of the runtime specification.
// A synchronous function would never return as it leads to a deadlock
// between the current job and a future job that is required to run to complete
// after the task is completed.
// Async forces a promise to be returned essentially clearing the job queue
// so other work can be done.
// The promise returned from async is not immediately executed but put into the
// job queue.

// for example:
// function sync_func() {
    // let promise = new Promise( (req, err ) => {
    //     console.log("setting up timeout 1")
    //     //setTimeout(() => req("done!"), 1000)
    //     req(10);
    //     err(10);
    // }).catch( (err) => { console.log("error: "+err) })
    // let result = await promise;
    // // this is now undefined as req was not passed anything
    // console.log(result);
// }

// how can you then do this? Immediately Invoked Function Expression (IIFE)
// aka Self-executing anonymous functions
// https://developer.mozilla.org/en-US/docs/Glossary/IIFE
function sync_func() {
    let promise = new Promise( (req) => {
        console.log("setting up timeout 1")
        setTimeout(() => req("done!"), 1000)
    })
    let result = null;
    ( async() => {
        result = await promise
        console.log(result);
    })();
    // this is now undefined as req was not passed anything
    console.log(result);
    
}
sync_func();