// Question 1 : can you have await without wrapping it in an async function?
// const response = await fetch("https://www.google.com")

let fetch = require("node-fetch")

// Question 2: What is wrong with this?
async function get_webpage() {
    return await fetch("https://www.google.com")
}

const x = get_webpage()
console.log(x)
x.then((data) => console.log(data));

async function get_webpage_2() {
    // promise is "activated" on assignment
    const data = await fetch("https://www.google.com")
    console.log(data)
}

get_webpage_2();
    