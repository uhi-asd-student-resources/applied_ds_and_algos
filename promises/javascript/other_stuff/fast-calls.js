async function getName2() {
    // console.log(Date.now())
    let ii  = 0
    while( ii < 1000000000 ) {
      ii++
    }
    return "Jill"
    
    // setTimeout( () => "Jill", 5000 );
  }
  
  async function getSurname2() {
    // console.log(Date.now())
    let ii  = 0
    while (ii < 1000000000) {
      ii++
    }
    return "Anderson"
    
    // setTimeout( () => "Anderson", 5000 );
  }
  
  // here the calls are done simultaneously
  async function getFullName2() {
    console.log(Date.now())
    let [ firstname, surname ] = await Promise.all( [getName2(), getSurname2()] );
    console.log(Date.now())
    console.log(`${firstname} ${surname}`)
  }
  
  getFullName2();
  