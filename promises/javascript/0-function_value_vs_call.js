
function print_function(f) {
    console.log(f)
    console.log(typeof(f))
    if ( typeof(f) === "function") {
        console.log(f())
    }

}

// declare a function and ensure it is in the global scope
global.print_function = function (f) {
    console.log(f)
    console.log(typeof(f))
    if ( typeof(f) === "function") {
        console.log(f())
    }

}

// another example with the global scope
print_function_2 = function (f) {
    console.log(f)
    console.log(typeof(f))
    if ( typeof(f) === "function") {
        console.log(f())
    }

}


function longTask() {
    console.log("done!")
}

// example of printing our an array
let a  = Array(1,2,3)
print_function(a)
print_function("a")

// send the function as a value
print_function(longTask)
// send the result of a function call
print_function(longTask())
// send the anonymous function as a value
print_function(() => {
    return 20;
})
// send the result of an anonymous function call
print_function((() => {
    return 20;
})())

// build a function name up as a string and
// then execute it
let function_to_call = "print"
let function_to_call_arg = "function"
let fname = function_to_call+"_"+function_to_call_arg
console.log(fname)
global[fname](longTask)
global[fname+"_2"](longTask)
console.log(global[fname])
if ( global[fname] ) {
    global[fname]()
}

// testing to see if a function can be called in Global
try {
    fname = "not_here"
    global[fname](longTask)
} catch( err ) {
    console.log("function "+fname+" was not found in Global scope")
}

// another way to test if the function or variable fname exists in global scope
if ( global[fname] ) {
    global[fname]()
}
