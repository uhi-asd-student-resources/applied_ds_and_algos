const { resolve } = require("path");

async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    return n;
}

async function longerTask() {
    let a = await longTask(100)
    //console.log(a)
    let b = await longTask(50)
    //console.log(b)
    let c = a + b
    //console.log(c)
    return c
}

// an equivalent to this might be, this is pretty yuck and 
// works, but you have do a lot of nasty code to make it work.
// The challenge is carrying arbitrary values forward through the logic.
async function simulation_of_async_longerTask() {
    return longTask(100)
    .then(
        (res) => { 
            console.log("stage 1: " +res)
            let f = { a: res }
            return f;
        })
    .then( (res) => {
            const a = res.a;
            longTask(50).then(
                (res2) => { 
                    console.log("stage 2: " +res2)
                    c = a + res2 
                    console.log("c="+c)
                    res.c = c
                    // at this point its non-obvious how to get
                    // the value of c back as a return value to this 
                    // the original promise.
            })
            return res
        }
    )
}


// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
longerTask().then(res => {
    console.log("This is the result of our async/await version:"+res)
    console.log("after instruction "+Date.now())
})


console.log("before simulation "+Date.now())
simulation_of_async_longerTask().then(res => {
    console.log("This is the result of our simulation: " +res.c)
    console.log("after instruction "+Date.now())
})
console.log