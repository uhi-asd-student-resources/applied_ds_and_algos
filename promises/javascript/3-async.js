// async wraps the rest in a promise
async function longTask() {         // <---- SYNTACTIC SUGAR - the good stuff!!
    let ii=0; 
    while( ii < 5000000000) {
        ii++;
    }
    console.log("done!")
}

// same as this but its nicer syntax
function longTask_version2() {
    return new Promise(() => {
        let ii=0; 
        while( ii < 5000000000) {
            ii++;
        }
        console.log("done!")
    });
}


function do_something_else() {
    console.log("after instruction "+Date.now())
}
// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
longTask()
.then(
    () => console.log("after instruction "+Date.now())
).then(
    function () { console.log("after instruction "+Date.now()) }
).then(
    do_something_else
)

console.log("before instruction "+Date.now())
longTask_version2()
.then(
    () => console.log("after instruction "+Date.now())
).then(
    function () { console.log("after instruction "+Date.now()) }
).then(
    do_something_else
)