// normal function definition
function first_form() {
    // do something
    console.log("1")
}

first_form()

// anonymous function call
const second_form = function() {
    // do_something
    console.log("2")
}
second_form(); // call

const third_form = () => {
    // do something
    console.log("3")
}
third_form(); // call


// the () INVOKES the function
// without the () you simply have a function object 
// which can be passed as a value to variables and arguments alike