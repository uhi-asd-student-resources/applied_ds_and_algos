// Introducing await...
// "The await operator is used to wait for a Promise. It can only be used inside an async function."
// Note that the await operator does NOT wait until the async completes before moving on.
// This is a subtle difference.

async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    return n;
}

async function longerTask() {
    let a = await longTask(100)
    //console.log(a)
    // a is a promise to run longTask
    let b = await longTask(50)
    //console.log(b)
    // b is a promise to run longTask
    let c = a + b
    // c = {promise1, promise}.then(+)
    //console.log(c)
    return c
}

// an equivalent to this might be:
async function simulation_of_async_longerTask() {
    return longTask(100).then(
        (res) => { b = longTask(50); return { a: res, b: b }}
    ).then(
        (res) => { return res.a + res.b }
    )
}



// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
console.log(longerTask())
console.log("after instruction "+Date.now())


console.log("before simulation "+Date.now())
console.log(simulation_of_async_longerTask())
console.log("after simulation "+Date.now())
