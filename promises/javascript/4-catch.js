// we still have our long task here
async function longTask() {
    let ii=0; 
    while( ii < 5000000000) {
        ii++;
    }
    throw new Error("ahhhhh!!!!");
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
longTask()
.then(
    () => console.log("after instruction "+Date.now())
);

// expecting this to throw an exception ...