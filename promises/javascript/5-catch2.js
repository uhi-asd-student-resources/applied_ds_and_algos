// same old long task
async function longTask() {
    throw new Error("ahhhhh!!!!");
    let ii=0; 
    while( ii < 50000000) {
        ii++;
    }
    throw new Error("ahhhhh!!!!");
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
// in this case the catch is not called because it is processed BEFORE the 
// Promise is called
try {
    console.log("before instruction "+Date.now())
    longTask()
    .then(
        () => console.log("after instruction "+Date.now())
    );
} catch(e) {
    console.log("caught you!")
}
console.log("after catch")

// expecting this to throw an exception ...
// the exception is called here after so its not called within the try...catch clause.