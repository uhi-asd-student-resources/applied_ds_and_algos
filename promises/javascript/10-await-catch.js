// Idea 1: why not put a try...catch loop in the function...

async function longTask(n) {
    let ii=0; 
    while( ii < 500000000) {
        ii++;
    }
    if ( n == 50 ) {
        throw new Error("invalid value")
    }
    return n;
}

async function longerTask() {
    try {
        let a = await longTask(100)
        let b = await longTask(50)
        let c = a + b
        return c
    } catch( err ) {
        console.log("caught in longerTask!")
        // more interesting stuff 
        // and still return something useful
        // By existing this function via the lower curly bracket
        // "the positive route" if you will, we are saying
        // this function was successful.  But it wasn't!
    }
}

// here after instruction happens BEFORE the end of longTask
// ... thats annoying....
console.log("before instruction "+Date.now())
longerTask().then(res => {
    // this is called when we don't want it to be.
    console.log(res)
    console.log("after instruction "+Date.now())
})
