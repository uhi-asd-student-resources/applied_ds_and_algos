#!/bin/bash

STRING_LENGTH=$1
if [ "x$STRING_LENGTH" == "x" ]; then
    STRING_LENGTH=8
fi

if [ ! -e "crypt_blowfish-1.3/wrapper.o" ]; then
    echo "Making blowfish and bcrypt"
    cd crypt_blowfish-1.3
    make
    cd ..
fi

echo "Compiling"
gcc --verbose -g -std=c99 test_hash_function.c ./crypt_blowfish-1.3/*.o -I./crypt_blowfish-1.3 -lcrypto -lssl -lm -o test_hash
if [ $? -ne 0 ]; then
    echo "Error compiling demo"
    exit 1
fi
echo "Run string hash tests"
echo "--------------------------------------------"
echo "Identical value"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --string --hash identical
echo "--------------------------------------------"
echo "Copy of input"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --string --hash copy
echo "--------------------------------------------"
echo "MD5"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --string --hash md5

# NOTE(tom) By default bcrypt is not enabled as it takes so long to run...
# echo "--------------------------------------------"
# echo "bcrypt"
# echo "--------------------------------------------"
# ./test_hash ${STRING_LENGTH} --string --hash bcrypt



echo
echo
echo "Run numeric hash tests"

echo "--------------------------------------------"
echo "Sum string characters"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --numeric --hash string-sum
echo "--------------------------------------------"
echo "Peter Weinbergers Hash Function"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --numeric --hash wein
echo "--------------------------------------------"
echo "Unix Elf Hash Function"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --numeric --hash elf
echo "--------------------------------------------"
echo "SDBM Hash Function"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --numeric --hash sdbm
echo "--------------------------------------------"
echo "Bernstein djb2 Hash Function"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --numeric --hash bern
echo "--------------------------------------------"
echo "Java string Hash Function"
echo "--------------------------------------------"
./test_hash ${STRING_LENGTH} --numeric --hash java
