#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <openssl/md5.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "crypt_blowfish-1.3/ow-crypt.h"
#define VERBOSE 0

struct LinkedList
{
    unsigned long hash;
    char *data;
    unsigned long counter;
    struct LinkedList *next;
};
typedef struct LinkedList LinkedList;

typedef struct
{
    unsigned long num_entries;
    unsigned long num_collisions;
} Statistics;

char *strdup(char const *src)
{
    char *dest = (char *)malloc(sizeof(char) * (strlen(src) + 1));
    // Why is this not secure?
    strcpy(dest, src);
    return dest;
}

void add_hash_to_linked_list(unsigned long value, LinkedList *head)
{
    if (head->hash == value)
    {
        head->counter++;
        return;
    }
    while (head->next != NULL)
    {
        if (head->hash == value)
        {
            head->counter++;
            return;
        }
        head = head->next;
    }
    // if we are here then we are at the end of the list
    // and we need to create a new item
    LinkedList *tail = (LinkedList *)malloc(sizeof(LinkedList));
    tail->hash = value;
    tail->data = NULL;
    tail->counter = 1;
    tail->next = NULL;
    head->next = tail;
}

void add_string_to_linked_list(char *value, LinkedList *head)
{
    if (head->data == NULL)
    {
        head->data = strdup(value);
        head->counter = 1;
        return;
    }
    if (strcmp(head->data, value) == 0)
    {
        head->counter++;
        return;
    }
    while (head->next != NULL)
    {
        assert(head->data != NULL);
        if (strcmp(head->data, value) == 0)
        {
            head->counter++;
            return;
        }
        head = head->next;
    }
    // if we are here then we are at the end of the list
    // and we need to create a new item
    LinkedList *tail = (LinkedList *)malloc(sizeof(LinkedList));
    tail->data = strdup(value);
    tail->counter = 1;
    tail->next = NULL;
    head->next = tail;
}

void linkedlist_print(LinkedList *item)
{
    while (item != NULL)
    {
        if (item->data == NULL)
        {
            printf("<NULL>\n");
        }
        else
        {
            printf("%s:%ld\n", item->data, item->counter);
        }
        item = item->next;
    }
}

unsigned long linkedlist_length(LinkedList *item)
{
    unsigned long length = 0;
    while (item != NULL)
    {
        length++;
        item = item->next;
    }
    return length;
}

void linkedlist_gather(LinkedList *item, Statistics *statistics)
{
    if (item == NULL)
    {
        return;
    }
    while (item != NULL)
    {
        statistics->num_entries += item->counter;
        statistics->num_collisions += item->counter - 1;
        item = item->next;
    }
}

typedef void (*string_hash_function)(char const *input, char *output);
typedef unsigned long (*numeric_hash_function)(char const *input);

// dummy function that returns the input
void string_hash_function_copy(char const *input, char *output)
{
    strcpy(output, input);
}

void string_hash_function_identical(char const *input, char *output)
{
    output[0] = 's';
    output[1] = '\0';
}

void str2md5(const char *str, char *output)
{
    int length = strlen(str);
    int n;
    MD5_CTX c;
    unsigned char digest[16];
    MD5_Init(&c);

    while (length > 0)
    {
        if (length > 512)
        {
            MD5_Update(&c, str, 512);
        }
        else
        {
            MD5_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    for (n = 0; n < 16; ++n)
    {
        snprintf(&(output[n * 2]), 16 * 2, "%02x", (unsigned int)digest[n]);
    }
}

// Reference: https://github.com/rg3/libbcrypt/blob/master/bcrypt.c
#define BCRYPT_HASHSIZE (64)
#define RANDBYTES (16)
static char salt[BCRYPT_HASHSIZE];

void bcrypt(const char *str, char *output)
{
    if (salt[0] == '\0')
    {
        printf("Generating salt...\n");
        int fd;
        char input[RANDBYTES];
        int workf;
        char *aux;

        fd = open("/dev/urandom", O_RDONLY);
        if (fd == -1)
        {
            printf("failed to open /dev/urandom\n");
            exit(1);
        }

        int total = 0;
        int partial = 0;
        while (total < RANDBYTES)
        {
            for (;;)
            {
                errno = 0;
                partial = read(fd, input + total, RANDBYTES - total);
                if (partial == -1 && errno == EINTR)
                    continue;
                break;
            }
            if (partial < 1)
            {
                printf("failed to get bytes\n");
                exit(1);
            }
            total += partial;
        }

        close(fd);

        /* Generate salt. */
        int factor = 12;
        workf = (factor < 4 || factor > 31) ? 12 : factor;
        crypt_gensalt_rn("$2a$", workf, input, RANDBYTES, salt, BCRYPT_HASHSIZE);
    }

    char *value = crypt_rn(str, salt, output, BCRYPT_HASHSIZE);
    strcpy(output, value);
    // printf("%s\n", value);
    //free(value);
}

unsigned long string_sum_hash(char const *input)
{
    unsigned long sum = 0;
    for (; *input; input++)
    {
        sum += *input;
    }
    return sum;
}

unsigned long peter_weinbergers_hash(char const *input)
{
    unsigned long h = 0, g;
    for (; *input; input++)
    {
        h = (h << 4) + *input;
        if ((g = (h & 0xF0000000)))
        {
            h ^= g >> 24;
            h ^= g;
        }
    }
    return h % 211;
}

unsigned long unix_elf_hash(char const *input)
{
    unsigned long h = 0, g;
    while (*input)
    {
        h = (h << 4) + *input++;
        if ((g = (h & 0xF0000000)))
        {
            h ^= g >> 24;
            h ^= ~g;
        }
    }
    return h;
}

unsigned long sdbm_hash(char const *input)
{
    unsigned long hash = 0;
    int c;
    while ((c = *input++))
    {
        hash = c + (hash << 6) - hash;
    }
    return hash;
}

unsigned long dan_bernstein_djb2_hash(char const *input)
{
    unsigned long hash = 5381;
    int c;
    while ((c = *input++))
    {
        hash = ((hash << 5) + hash) + c;
    }
    return hash;
}

unsigned long java_hashcode(char const *input)
{
    unsigned long hash = 0;
    int n = strlen(input) - 1;
    while (*input)
    {
        hash += (unsigned long)((double)*input++ * pow(31, (double)n));
        n--;
    }
    return hash;
}

#define HASH_MODE_STRING 0
#define HASH_MODE_NUMERIC 1

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("you must specify the length of string to generate.\n");
        exit(1);
    }
    int string_length = atoi(argv[1]);
    string_hash_function shf = string_hash_function_copy;
    numeric_hash_function nhf = string_sum_hash;

    if (string_length < 1)
    {
        printf("length must be greater than 0\n");
        exit(1);
    }
    int hash_mode = 0;
    if (argc > 3)
    {
        if (strcmp(argv[2], "--string") == 0)
        {
            hash_mode = HASH_MODE_STRING;
        }
        else if (strcmp(argv[2], "--numeric") == 0)
        {
            hash_mode = HASH_MODE_NUMERIC;
        }
    }
    if (argc == 4)
    {
        printf("missing hash function name\n");
        exit(1);
    }
    if (argc > 4)
    {
        if (strcmp(argv[3], "--hash") == 0)
        {
            char *hash_name = argv[4];
            nhf = NULL;
            shf = NULL;
            if (strcmp(hash_name, "copy") == 0)
            {
                shf = string_hash_function_copy;
            }
            if (strcmp(hash_name, "identical") == 0)
            {
                shf = string_hash_function_identical;
            }
            if (strcmp(hash_name, "md5") == 0)
            {
                shf = str2md5;
            }
            if (strcmp(hash_name, "bcrypt") == 0)
            {
                shf = bcrypt;
            }
            if (strcmp(hash_name, "string-sum") == 0)
            {
                nhf = string_sum_hash;
            }
            if (strcmp(hash_name, "wein") == 0)
            {
                nhf = peter_weinbergers_hash;
            }
            if (strcmp(hash_name, "elf") == 0)
            {
                nhf = unix_elf_hash;
            }
            if (strcmp(hash_name, "sdbm") == 0)
            {
                nhf = sdbm_hash;
            }
            if (strcmp(hash_name, "bern") == 0)
            {
                nhf = dan_bernstein_djb2_hash;
            }
            if (strcmp(hash_name, "java") == 0)
            {
                nhf = java_hashcode;
            }
            if (hash_mode == HASH_MODE_NUMERIC)
            {
                if (nhf == NULL)
                {
                    printf("numeric hash function '%s' not recognised\n", hash_name);
                    exit(1);
                }
            }
            else
            {
                if (shf == NULL)
                {
                    printf("string hash function '%s' not recognised\n", hash_name);
                    exit(1);
                }
            }
        }
    }
    if (string_length < 2)
    {
        printf("length must be greater than 0\n");
        exit(1);
    }
    printf("Mode: %s\n", (hash_mode ? "numeric" : "string"));
    char *buffer = (char *)malloc(string_length + 1); // add one for the \0 byte at the end of the string
    // why can we use char here?
    char *index = (char *)malloc(string_length + 1); // add one for the \0 byte at the end of the string
    char const *valid_chars = "abc";                 // defghijklmnopqrstuvwxyz0123456789ABCDFEGHIJKLMNOPQRSTUVWXYZ!£$%^&*#~@:;?<>,. ";
    int num_letters = strlen(valid_chars);
    printf("Alphabet size: %d\n", num_letters);
    int hash_table_size = 4096; // an example value
    char output[4096];

    LinkedList *table = (LinkedList *)malloc(sizeof(LinkedList));
    LinkedList **hash_table = (LinkedList **)malloc(sizeof(LinkedList *) * hash_table_size);

    // hash_table begins as an array of NULLs
    memset(hash_table, 0, hash_table_size);

    table->data = NULL;
    table->counter = 0;
    table->next = NULL;

    memset(buffer, 0, string_length + 1);
    memset(index, 0, string_length + 1);

    // brute force string generation
    // each character can be one of num_letters
    // total combinations is sum_{ii=1}^{ii=string_length} num_letters^ii
    // e.g. for alphabet of 3 letters and a string length of 3 we have
    // 3^3 + 3^2 + 3 items
    for (int current_string_length = 0; current_string_length < string_length; current_string_length++)
    {
        // clear our memory to all '\0's
        int smallest_index_to_update = 0;
        memset(index, 0, string_length + 1);
        memset(buffer, 0, string_length + 1);
        memset(buffer, valid_chars[0], current_string_length + 1);
        do
        {
            // moving from right to left
            // we increment the value 1 at a time
            // if we increment and we get to the max
            // we set to 0 and increment the one before
            int dirty = 0;
            for (smallest_index_to_update = current_string_length;
                 smallest_index_to_update > -1;
                 smallest_index_to_update--)
            {
                if (index[smallest_index_to_update] >= num_letters)
                {
                    // here a value has reached the last letter of the alphabet
                    // so we reset to 0 and then allow the loop to continue
                    // to the next value to the left.
                    index[smallest_index_to_update] = 0;
                    buffer[smallest_index_to_update] = valid_chars[0];
                    if (smallest_index_to_update >= 0)
                    {
                        index[smallest_index_to_update - 1]++;
                        dirty = 1;
                    }
                    // smallest_index_to_update--;
                    // if ( smallest_index_to_update < 0 ) {
                    //     break;
                    // }
                    // while( smallest_index_to_update >= 0 && index[smallest_index_to_update] != num_letters ) {
                    // index[smallest_index_to_update]++;
                    // if ( index[smallest_index_to_update] >= num_letters ) {
                    //     smallest_index_to_update = -2;
                    //     break;
                    // }
                    // buffer[smallest_index_to_update] = valid_chars[index[smallest_index_to_update]];
                    // smallest_index_to_update--;
                    // }
                    //break;
                }
                else
                {
                    // use the letter and then increment the index
                    // we break out so we can do what we need to.
                    buffer[smallest_index_to_update] = valid_chars[index[smallest_index_to_update]];
                    if (!dirty)
                    {
                        index[smallest_index_to_update]++; // <-- this is a problem as we repeat when we roll over
                    }
                    else
                    {
                        dirty = 0;
                    }
                    break;
                }
            }

            // we only want to do something when we are incrementing the right most value
            // which is when smallest_index_to_update == current_string_length.
            // if we did not we would have repeated values in our output
            if (smallest_index_to_update == current_string_length)
            {
#if VERBOSE
                printf("buffer: %d, %s\n", smallest_index_to_update, buffer);
                printf("input : %s\n", buffer);
#endif
                if (hash_mode == HASH_MODE_STRING)
                {
                    shf(buffer, output);
#if VERBOSE
                    printf("output: %s\n", output);
#endif
                    add_string_to_linked_list(output, table);
                }
                else
                {
                    unsigned long hash = nhf(buffer);
                    unsigned long mod_hash = hash % hash_table_size; // we mod by expected table size
                    // printf("Hash: %ld,  mod %d: %ld\n", hash, hash_table_size, mod_hash);
                    if (hash_table[mod_hash] == NULL)
                    {
                        // this is the first time we have allocated to this hash space so
                        // we have to give it some memory to work with.
                        hash_table[mod_hash] = (LinkedList *)malloc(sizeof(LinkedList));
                        hash_table[mod_hash]->counter = 1;
                        hash_table[mod_hash]->data = NULL;
                        hash_table[mod_hash]->next = NULL;
                        hash_table[mod_hash]->hash = hash;
                    }
                    else
                    {
                        add_hash_to_linked_list(mod_hash, hash_table[mod_hash]);
                    }
                }
#if VERBOSE
                printf("length: %ld\n", linkedlist_length(table));
#endif
            }
        } while (smallest_index_to_update >= 0);
    }

    assert(table != NULL);

#if VERBOSE
    linkedlist_print(table);
    unsigned int n = linkedlist_length(table);
    printf("Linked list length: %d\n", n);
#endif

    // we want to calculate some stats on our table
    if (hash_mode == HASH_MODE_STRING)
    {
        Statistics statistics = {0, 0};
        linkedlist_gather(table, &statistics);
        printf("Linkedlist Statistics\n");
        printf("- number of entries: %ld\n", statistics.num_entries);
        printf("- number of collisions: %ld\n", statistics.num_collisions);
        printf("- average collisions: %0.2f%\n", (float)statistics.num_collisions / (float)statistics.num_entries * 100);
    }
    else
    {
        Statistics statistics = {0, 0};
        int hash_entries_used = 0;
        for (int table_index = 0; table_index < hash_table_size; table_index++)
        {
            if (hash_table[table_index] != NULL)
            {
                linkedlist_gather(hash_table[table_index], &statistics);
                hash_entries_used++;
            }
        }
        printf("HashTable Statistics\n");
        printf("- hash table size: %d\n", hash_table_size);
        printf("- hash table entries used: %d\n", hash_entries_used);
        printf("- number of entries: %ld\n", statistics.num_entries);
        printf("- number of collisions: %ld\n", statistics.num_collisions);
        printf("- average collisions: %0.2f%\n", (float)statistics.num_collisions / (float)statistics.num_entries * 100.0f);
    }
    free(buffer);
    free(table);
    return 0;
}