# Fast Inverse Square Root algorithm

This came from a student question.  You can read about it here: https://en.wikipedia.org/wiki/Fast_inverse_square_root.

The instruction rsqrtss on x86 is better for this now so you should not need to use this type of code.

**Warning: This invokes undefined behaviour.**