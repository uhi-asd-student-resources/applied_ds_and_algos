#include <stdint.h>
#include <stdio.h>

// code is from https://en.wikipedia.org/wiki/Fast_inverse_square_root
// Note the use of floating point numbers here, this is common in 
// the domain that this type of function is used. i.e. 3d games.
float Q_rsqrt( float number )
{	
	const float x2 = number * 0.5f;
	const float threehalfs = 1.5f;

	// note this is union allows us to interpret
	// the input value as EITHER a FLOAT or an UNSIGNED INTEGER.
	union {
		float f;
		uint32_t i;
	} conv  = { .f = number };
	// here we interpret the value as an unsigned integer and 
	// use the magic number 0x5f3759df.  You can read more about
	// where this number comes from in the wiki article.
	// Magic numbers are not good for maintainable code so should be avoided.
	// If you have to use them, then you should document why and how the 
	// magic numbers were found.
	// This is approximately the inverse square root of the input number.
	conv.i  = 0x5f3759df - ( conv.i >> 1 );
	// we then re-interpret this unsigned integer as a floating point
	// value.  This is undefined behaviour.
	// This is bad for portability and it assumes that the compiler
	// will "do the right thing" whenever this code is generated under
	// all optimisations - not guaranteed.
	// There are ways to do this that is not undefined behaviour.
	conv.f  *= threehalfs - ( x2 * conv.f * conv.f );
	return conv.f;
}

int main() {
    float y = Q_rsqrt(10.5);
    printf("%f\n", y);
}
